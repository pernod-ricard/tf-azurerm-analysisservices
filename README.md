Example :  
```
module "analysisservices" {
  source = "bitbucket.org/pernod-ricard/tf-azurerm-analysisservices"
  allow_power_bi  = true
  environment     = "dev"
  location        = "northeurope"
  pricing_tier    = "B1"
  project_entity  = "PRH"
  project_name    = "projectX"
}
```