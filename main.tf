locals {
  analysis_name = replace(var.project_name, " ", "")
}

resource "azurerm_analysis_services_server" "server" {
  name                    = lower("sas${var.project_entity}${local.analysis_name}${var.environment}")
  location                = var.azure_region
  resource_group_name     = var.resourcegroup_name
  sku                     = var.pricing_tier
  admin_users             = []
  enable_power_bi_service = var.allow_power_bi

  lifecycle {
    ignore_changes = [tags, admin_users, ipv4_firewall_rule]
  }
}

/*
Need to understand the purpose of the following...
data "azuread_service_principal" "service-connection" {
  application_id = "ee0d2f87-1558-4403-876c-facf9a84c97b" // funprhshared02powershellprod
}

resource "azurerm_role_assignment" "devops-contributor" {
  scope = azurerm_analysis_services_server.server.id
  role_definition_name = "Contributor"
  principal_id = data.azuread_service_principal.service-connection.object_id
}
*/